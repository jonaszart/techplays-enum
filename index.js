const countryEnum = ['Israel', 'Switzerland', 'Russia', 'United Kingdom', 'Singapore',
'Germany', 'United States', 'South Korea', 'China', 'Japan'];

// const roleEnum = ['scientist', 'engineer', 'businessman', 'politician'];

const roleEnum = {
	SCIENTIST: 'scientist',
	ENGINEER: 'engineer',
	BUSINESSMAN: 'businessman',
	POLITICIAN: 'politician'
};

module.exports = {
  countryEnum,
  roleEnum
};
