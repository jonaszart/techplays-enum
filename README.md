# techplays-enum

Pacote dedicado à armazenar enumeradores para techplays, ideal para utilização nas models do mongoose e no schema das APIs

## Instalação

`npm install techplays-enum`

## Utilização

A interface expõe os enumeradores no arquivo inicial, basta desconstrui-los pelo nome:

```
const { countryEnum } = require('techplays-enum');
```
